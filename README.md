# nextcloud-docker

This script will download and deploy nextcloud as docker services on a swarm
environment.  

We will be setting up a MariaDB container, the Nextcloud fpm container, 
a redis container and
a Nginx container running as a web server.  All external storage will be
defined as docker volumes in the compose.yml file.  Two networks are defined:
The first is for communication between the MariaDB server and the NextCloud
container.  The second is for communication between the nginx container and
the Nextcloud fpm container.  This allows us to use the database for other
services as well as upgrade the components without breaking the "--link"
between them.

Images pulled from hub.docker.com have their versions specified as I don't 
like to be surprised with a full version upgrade.  You will need to check for
new releases manually.

nginx will listen on ports 80 and 443.  Communication to the nginx fpm
container will be proxied by the nginx configuration.  The static web data
is served by nginx from 

ToDo:
* ~~there are still some network name resolution issues between containers.~~
* Add LetsEncrypt configuration to the nginx configuration.
* clean up and add comments