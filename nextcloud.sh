#!/bin/bash

# This script will download and deploy nextcloud as docker services on a swarm
# environment.  

#=========================================================================

# PATHS
# NOTE: These paths *must* match those found in compose.yml.  
DATAROOT=/data
DBDATA=$DATAROOT/msql-data
NC_HTML=$DATAROOT/nextcloud-html
BUDIR=$DATAROOT/backups
NGINX_DATA=$DATAROOT/nginx
CERTPATH=$NGINX_DATA/certs

# constants
STKNAME="NCSTACK"
DBIMG="mariadb:10.4"
NCIMG="nextcloud:17-fpm-alpine"
NGNXIMG="nginx:1.17-alpine"
REDIMG="redis:5-alpine"

pull_imgs() {
    docker pull $DBIMG
    docker pull $NCIMG
    docker pull $NGNXIMG
    docker pull $REDIMG
}

check_swarm() {
    sw_status=$(docker info|grep Swarm|cut -d" " -f3)
    if [ "$sw_status" != "active" ]; then
        echo "Starting docker swarm"
        docker swarm init
    else
        echo "Swarm is $sw_status"
    fi
}

init_db() {
    if [ ! -d $DBDATA/performance_schema ]; then
        echo
        pwconfirmed=0
        while [ $pwconfirmed -eq 0 ]
        do
            read -s -p "Enter database root password: " DBROOTPW
            echo
            read -s -p "Confrirm database root password: " CONFPW
            echo
            if [ $CONFPW == $DBROOTPW ]; then
                pwconfirmed=1
            else
                echo "Passwords do not match.  Try again..."
            fi
        done
        echo
        read -p "Enter the name for the NextCloud database: " NXCLD_DB
        read -p "Enter the name for the NextCloud user account: " NXCLD_USER
        pwconfirmed=0
        while [ $pwconfirmed -eq 0 ]
        do
            read -s -p "Enter database password for ncuser: " NCUSERPW
            echo
            read -s -p "Confrirm database password for ncuser: " CONFPW
            echo
            if [ $CONFPW == $NCUSERPW ]; then
                pwconfirmed=1
            else
                echo "Passwords do not match.  Try again..."
            fi
        done
        echo
    fi
    docker run -d \
           --rm \
           --name dbInit \
           -v $DBDATA:/var/lib/mysql \
           -e MYSQL_ROOT_PASSWORD=$DBROOTPW \
           $DBIMG
    echo "Waiting 60 seconds for mariadb to start up..."
    sleep 60

    echo "Adding the NextCloud database and user account to MariaDB..."
    docker exec -t dbInit sh -c "mysql -u root -p$DBROOTPW <<EOT
CREATE USER '$NXCLD_USER'@'%' IDENTIFIED BY '$NCUSERPW';
SELECT User, Host FROM mysql.user;
CREATE DATABASE $NXCLD_DB;
GRANT ALL PRIVILEGES ON $NXCLD_DB.* TO '$NXCLD_USER'@'%';
FLUSH PRIVILEGES;
EOT
"
    echo "The temporary container will exit and be deleted in 10 seconds."
    sleep 10
    docker stop dbInit
    echo "done."
}

check_db_paths() {
    echo "Checking database paths..."
    if [ ! -d $DBDATA ]; then
        echo creating directory $DBDATA
        mkdir -p $DBDATA
    fi
}


check_nextcloud_paths() {
    echo "Checking nextcloud paths..."
    for xpath in $NC_HTML $BUDIR
    do
	if [ ! -d $xpath ]; then
            echo creating directory $xpath
            mkdir -p $xpath
	fi
    done
}

check_nginx_paths() {
    echo "Checking nginx paths..."
    if [ ! -d $CERTPATH ]; then
        echo creating directory $CERTPATH
        mkdir -p $CERTPATH
    fi
    if [ ! -d $NGINX_DATA/conf.d ]; then
	echo copying over nginx configuration file...
	mkdir -vp $NGINX_DATA/conf
    fi
    rsync -av ./conf.d $NGINX_DATA/ --delete --progress
}

run_stack() {    
    echo "Starting stack $STKNAME"
    docker stack deploy -c ./compose.yml $STKNAME
}

stop_stack() {
    echo "Removing stack $STKNAME"
    docker stack rm $STKNAME
}

clean_stack() {
    echo "Removing stack related networks."
    for netname in mysql-net nextcld-net redis-net
    do
	docker network rm $netname
    done
    
    echo "Removing stack related volumes."
    for volname in NCSTACK_backups \
		       NCSTACK_msql-data \
		       NCSTACK_nc-html \
		       NCSTACK_nginxcerts \
		       NCSTACK_nginxconf
    do
	docker volume rm $volname
    done
}

usage() {
    echo "$0 start|stop|clean|initdb|help"
}


case $1 in
    initdb)
        check_db_paths
        init_db
        ;;
    start)
	check_swarm
	check_db_paths
	check_nextcloud_paths
	check_nginx_paths
        pull_imgs
        run_stack
        ;;
    update)
	run_stack
	;;
    stop)
        stop_stack
        ;;
    clean)
	stop_stack
	clean_stack
	;;
    *)
        usage
esac
